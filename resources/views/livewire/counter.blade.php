<div class="flex flex-row gap-4 items-center justify-center p-20">
    <button wire:click="increment" class="px-6 py-4 bg-red-800 text-white text-2xl font-bold rounded-lg hover:bg-red-700">+</button>
    <span class="px-6 py-4 text-2xl bg-gray-200 bg-opacity-40 rounded-lg">{{$count}}</span>
    <button  wire:click="decrement" class="px-6 py-4 bg-red-800 text-white text-2xl font-bold rounded-lg hover:bg-red-700">-</button>
</div>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <!-- Styles -->

    @livewireStyles
    @vite(['resources/css/app.css','resources/js/app.js'])
</head>
<body class="antialiased">
    <nav class="flex bg-gray-800 text-white mb-2">
        <a href="/"  class="py-4 px-6 hover:bg-gray-700 {{(request()->routeIs('home')? 'bg-gray-600' :"")}}">Home</a>
        <a href="/counter"  class="py-4 px-6 hover:bg-gray-700 {{(request()->routeIs('counter')? 'bg-gray-600' :"")}}">Counter</a>
        <a href="/todo-list" class="py-4 px-6 hover:bg-gray-700  {{(request()->routeIs('todo')? 'bg-gray-600' :"")}}">Todo</a>
    </nav>
    {{$slot}}

    @livewireScripts
</body>
</html>


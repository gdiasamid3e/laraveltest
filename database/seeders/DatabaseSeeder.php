<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Continet;
use App\Models\Country;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       $continents=[
         ['id'=>1,'name'=>'Europe'],
         ['id'=>2,'name'=>'Asia'],
         ['id'=>3,'name'=>'South Africa'],
         ['id'=>4,'name'=>'North America'],
         ['id'=>5,'name'=>'Africa'],
       ];

       foreach ($continents as $con){
            Continet::factory()->create($con)->each(function ($i){
                $i->countries()->saveMany(Country::factory(10)->make());
            });

       }
    }
}

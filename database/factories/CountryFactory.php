<?php

namespace Database\Factories;

use App\Models\Continet;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Country>
 */
class CountryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'=>fake()->country,
            'continet_id'=>fake()->numberBetween(1,Continet::count())
        ];
    }
}
